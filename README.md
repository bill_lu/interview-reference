## 最新 10000+ 道，每道题都配了答案，面试题，汇总

## 整理那么辛苦，求个 Star 谢谢！！

最近又赶上跳槽的高峰期，好多粉丝，都问我要有没有最新面试题，整理了一个多月，白天背着老板偷偷整理，晚上回家熬夜整理，终于整理好了，10000+ 道，刷完进大厂轻轻松松，我会持续更新，马上就会整理更多！

## 题库非常全面，累计 10000+ 道

包括 Java 集合、JVM、多线程、并发编程、设计模式、SpringBoot、SpringCloud、Java、MyBatis、ZooKeeper、Dubbo、Elasticsearch、Memcached、MongoDB、Redis、MySQL、RabbitMQ、Kafka、Linux、Netty、Tomcat、Python、HTML、CSS、Vue、React、JavaScript、Android 大数据、阿里巴巴等大厂面试题等、等技术栈！

## 面试题题库

整理中。。。。

## 互联网大厂面试真题（持续更新）

[1：阿里巴巴Java面试题](http://39sd.cn/202203291)

[2：阿里云Java面试题-实习生岗](http://39sd.cn/202203292)

[3：腾讯Java面试题-高级](http://39sd.cn/202203293)

[4：字节跳动Java面试题](http://39sd.cn/202203294)

[5：字节跳动Java面试题-大数据方向](http://39sd.cn/202203295)

[6：百度Java面试题](http://39sd.cn/202203252)

[7：蚂蚁金服Java面试题-中级](http://39sd.cn/202203297)

[8：蚂蚁金服Java面试题-高级](http://39sd.cn/202203298)

[9：京东Java面试题-中级](http://39sd.cn/202203299)

[10：拼多多Java面试题-电商部](http://39sd.cn/2022032910)

[11：商汤科技Java面试题](http://39sd.cn/2022032911)

[12：中软国际Java面试题-初级](http://39sd.cn/2022032912)

[13：唯品会Java面试题](http://39sd.cn/2022032913)

[14：携程Java面试题-高级](http://39sd.cn/2022032914)

[15：软通动力Java面试题](http://39sd.cn/2022032915)