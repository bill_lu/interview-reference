**SpringBoot经典面试题及答案**，每道都是认真筛选出的大厂高频面试题，助力大家能找到满意的工作！

### **下载链接**：[**全部面试题及答案PDF**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)


### [1.什么是 spring boot？](http://39sd.cn/2022010501)

spring boot 是为 spring 服务的，是用来简化新 spring 应用的初始搭建以及开发过程的。

### [2.为什么要用 spring boot？](http://39sd.cn/2022010501)

•	配置简单

•	独立运行

•	自动装配

•	无代码生成和 xml 配置

•	提供应用监控

•	易上手

•	提升开发效率

### [3.spring boot 核心配置文件是什么？](http://39sd.cn/2022010501)

spring boot 核心的两个配置文件：

•	bootstrap (. yml 或者 . properties)：boostrap 由父 ApplicationContext 加载的，
比 applicaton 优先加载，且 boostrap 里面的属性不能被覆盖；

•	application (. yml 或者 . properties)：用于 spring boot 项目的自动化配置。

### [4.spring boot 配置文件有哪几种类型？它们有什么区别？](http://39sd.cn/2022010501)

配置文件有 . properties 格式和 . yml 格式，它们主要的区别是书法风格不同。
 
. properties 配置如下：

spring. RabbitMQ. port=5672

. yml 配置如下：

spring:
    RabbitMQ:
        port: 5672

. yml 格式不支持 @PropertySource 注解导入。

### [5.spring boot 有哪些方式可以实现热部署？](http://39sd.cn/2022010501)

•	使用 devtools 启动热部署，添加 devtools 库，在配置文件中把 spring. devtools. restart. enabled 设置为 true；

•	使用 Intellij Idea 编辑器，勾上自动编译或手动重新编译。

### [6.jpa 和 hibernate 有什么区别？](http://39sd.cn/2022010501)

jpa 全称 Java Persistence API，是 Java 持久化接口规范，hibernate 属于 jpa 的具体实现。

### [7.什么是 JavaConfig？](http://39sd.cn/2022010501)

Spring JavaConfig 是 Spring 社区的产品，它提供了配置 Spring IoC 容器的纯 Java 方法。因此它有助于避免使用 XML 配置。使用 JavaConfig 的优点在于：

面向对象的配置。由于配置被定义为 JavaConfig 中的类，因此用户可以充分利用 Java 中的面向对象功能。一个配置类可以继承另一个，重写它的@Bean 方法等。

减少或消除 XML 配置。基于依赖注入原则的外化配置的好处已被证明。但是，许多开发人员不希望在 XML 和 Java 之间来回切换。JavaConfig 为开发人员提供了一种纯 Java 方法来配置与 XML 配置概念相似的 Spring 容器。从技术角度来讲，只使用 JavaConfig 配置类来配置容器是可行的，但实际上很多人认为将 JavaConfig 与 XML 混合匹配是理想的。

类型安全和重构友好。JavaConfig 提供了一种类型安全的方法来配置 Spring 容器。由于Java	5.0 对泛型的支持，现在可以按类型而不是按名称检索 bean，不需要任何强制转换或基于字符串的查找。

### [8.如何重新加载 Spring Boot 上的更改，而无需重新启动服务器？](http://39sd.cn/2022010501)

这可以使用 DEV 工具来实现。

通过这种依赖关系，您可以节省任何更改，嵌入式 tomcat将重新启动。Spring	Boot 有一个开发工具（DevTools）模块，它有助于提高开发人员的生产力。

Java 开发人员面临的一个主要挑战是将文件更改自动部署到服务器并自动重启服务器。开发人员可以重新加载 Spring Boot 上的更改，而无需重新启动服务器。这将消除每次手动部署更改的需要。

Spring Boot 在发布它的第一个版本时没有这个功能。这是开发人员最需要的功能。DevTools 模块完全满足开发人员的需求。该模块将在生产环境中被禁用。它还提供 H2 数据库控制台以更好地测试应用程序。

org.springframework.boot spring-boot-devtools true

#### [9.Spring Boot 中的监视器是什么？](http://39sd.cn/2022010501)

Spring boot actuator 是 spring 启动框架中的重要功能之一。Spring boot 监视器可帮助您访问生产环境中正在运行的应用程序的当前状态。有几个指标必须在生产环境中进行检查和监控。即使一些外部应用程序可能正在使用这些服务来向相关人员触发警报消息。监视器模块公开了一组可直接作为 HTTP URL 访问的 REST 端点来检查状态。

### [10.如何在 Spring Boot 中禁用 Actuator 端点安全性？](http://39sd.cn/2022010501)

默认情况下，所有敏感的 HTTP 端点都是安全的，只有具有 ACTUATOR 角色的用户才能访问它们。安全性是使用标准的 HttpServletRequest.isUserInRole 方法实施的。 

我们可以使用management.security.enabled = false来禁用安全性。

只有在执行机构端点在防火墙后访问时，才建议禁用安全性。

### [11.如何在自定义端口上运行 Spring Boot 应用程序？](http://39sd.cn/2022010501)

为了在自定义端口上运行 Spring	Boot 应用程序，您可以在 application.properties 中指定端
口。

server.port = 8090

### [12.什么是 YAML？](http://39sd.cn/2022010501)

### [13.如何实现 Spring Boot 应用程序的安全性？](http://39sd.cn/2022010501)

### [14.如何集成 Spring Boot 和 ActiveMQ？](http://39sd.cn/2022010501)

### [15.如何使用 Spring Boot 实现分页和排序？](http://39sd.cn/2022010501)

### [16.什么是 Swagger？你用 Spring Boot 实现了它吗？](http://39sd.cn/2022010501)

### [17.什么是 Spring Profiles？](http://39sd.cn/2022010501)

### [18.什么是 Spring Batch？](http://39sd.cn/2022010501)

### [19.什么是 FreeMarker 模板？](http://39sd.cn/2022010501)

### [20.如何使用 Spring Boot 实现异常处理？](http://39sd.cn/2022010501)

### [21.您使用了哪些 starter maven 依赖项？ ](http://39sd.cn/2022010501)

### [22.什么是 WebSockets？](http://39sd.cn/2022010501)

### [23.什么是 AOP？](http://39sd.cn/2022010501)

### [24.什么是 Apache Kafka？](http://39sd.cn/2022010501)

### [25.我们如何监视所有 Spring Boot 微服务？](http://39sd.cn/2022010501)

### **下载链接**：[**博主已将以上这些面试题整理成了一个面试手册，是PDF版的**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)