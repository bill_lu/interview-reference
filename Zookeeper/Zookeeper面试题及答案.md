**Zookeeper面试题及答案**，每道都是认真筛选出的高频面试题，助力大家能找到满意的工作！

### **下载链接**：[**全部面试题及答案PDF**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)

### [1.ZooKeeper 是什么？](http://39sd.cn/202203071)

ZooKeeper 是一个分布式的，开放源码的分布式应用程序协调服务，是 Google 的 Chubby 一个开源的实现，它是集群的管理者，监视着集群中各个节点的状态根据节点提交的反馈进行下一步合理操作。最终，将简单易用的接口和性能高效、功能稳定的系统提供给用户。

客户端的读请求可以被集群中的任意一台机器处理，如果读请求在节点上注册了监听器，这个监听器也是由所连接的 zookeeper 机器来处理。对于写请求，这些请求会同时发给其他 zookeeper 机器并且达成一致后，请求才会返回成功。因此，随着 zookeeper 的集群机器增多，读请求的吞吐会提高但是写请求的吞吐会下降。有序性是 zookeeper 中非常重要的一个特性，所有的更新都是全局有序的，每个更新都有一个唯一的时间戳，这个时间戳称为 zxid（Zookeeper Transaction Id）。而读请求只会相对于更新有序，也就是读请求的返回结果中会带有这个 zookeeper 最新的 zxid。

### [2.Zookeeper 工作原理](http://39sd.cn/202203071)

Zookeeper 的核心是原子广播，这个机制保证了各个 Server 之间的同步。实现这个机制的协议叫做 Zab 协 议。Zab 协议有两种模式，它们分别是恢复模式（选主）和广播模式（同步）。当服务启动或者在领导者崩溃后，Zab 就进入了恢复模式，当领导者被选举出来，且大多数 Server 完成了和 leader 的状态同步以后，恢复模式就结束了。状态同步保证了 leader 和 Server 具有相同的系统状态。

### [3.Zookeeper 负载均衡和 nginx 负载均衡区别](http://39sd.cn/202203071)

zk 的负载均衡是可以调控，nginx 只是能调权重，其他需要可控的都需要自己写插件；但是 nginx 的吞吐量比zk 大很多，应该说按业务选择用哪种方式。

### [4.ZooKeeper 提供了什么？](http://39sd.cn/202203071)

1、文件系统

2、通知机制

### [5.Zookeeper 同步流程](http://39sd.cn/202203071)

选完 Leader 以后，zk 就进入状态同步过程。

1、Leader 等待 server 连接；

2、Follower 连接 leader，将最大的 zxid 发送给 leader； 

3、Leader 根据 follower 的 zxid 确定同步点；

4、完成同步后通知 follower 已经成为 uptodate 状态；

5、Follower 收到 uptodate 消息后，又可以重新接受 client 的请求进行服务了。

![](../img/Zookeeper6.png)

### [6.分布式通知和协调](http://39sd.cn/202203071)

对于系统调度来说：操作人员发送通知实际是通过控制台改变某个节点的状态，然后 zk 将这些变化发送给注册了这个节点的 watcher 的所有客户端。

对于执行情况汇报：每个工作进程都在某个目录下创建一个临时节点。并携带工作的进度数据，这样汇总的进程可以监控目录子节点的变化获得工作进度的实时的全局情况。

### [7.机器中为什么会有 leader？](http://39sd.cn/202203071)

在分布式环境中，有些业务逻辑只需要集群中的某一台机器进行执行，其他的机器可以共享这个结果，这样可以大大减少重复计算，提高性能，于是就需要进行 leader 选举。

### [8.zk 节点宕机如何处理？](http://39sd.cn/202203071)

Zookeeper 本身也是集群，推荐配置不少于 3 个服务器。Zookeeper 自身也要保证当一个节点宕机时，其他节点会继续提供服务。

如果是一个 Follower 宕机，还有 2 台服务器提供访问，因为 Zookeeper 上的数据是有多个副本的，数据并不会丢失；

如果是一个 Leader 宕机，Zookeeper 会选举出新的 Leader。

ZK 集群的机制是只要超过半数的节点正常，集群就能正常提供服务。只有在 ZK 节点挂得太多，只剩一半或不到一半节点能工作，集群才失效。

所以3 个节点的 cluster 可以挂掉 1 个节点(leader 可以得到 2 票>1.5)2 个节点的 cluster 就不能挂掉任何 1 个节点了(leader 可以得到 1 票<=1)

### [9.Zookeeper 文件系统](http://39sd.cn/202203071)

Zookeeper 提供一个多层级的节点命名空间（节点称为 znode）。与文件系统不同的是，这些节点都可以设置关联的数据，而文件系统中只有文件节点可以存放数据而目录节点不行。Zookeeper 为了保证高吞吐和低延迟，在内存中维护了这个树状的目录结构，这种特性使得 Zookeeper 不能用于存放大量的数据，每个节点的存放数据上限为 1M。

### [10.四种类型的 znode](http://39sd.cn/202203071)

1、PERSISTENT-持久化目录节点

客户端与 zookeeper 断开连接后，该节点依旧存在

2、PERSISTENT_SEQUENTIAL-持久化顺序编号目录节点

客户端与 zookeeper 断开连接后，该节点依旧存在，只是 Zookeeper 给该节点名称进行顺序编号

3、EPHEMERAL-临时目录节点

客户端与 zookeeper 断开连接后，该节点被删除

4、EPHEMERAL_SEQUENTIAL-临时顺序编号目录节点

客户端与 zookeeper 断开连接后，该节点被删除，只是 Zookeeper 给该节点名称进行顺序编号

![](../img/Zookeeper1.png)

### [11.Zookeeper 队列管理（文件系统、通知机制）](http://39sd.cn/202203071)

### [12.Zookeeper 数据复制](http://39sd.cn/202203071)

### [13.Zookeeper 通知机制](http://39sd.cn/202203071)

### [14.Zookeeper 做了什么？](http://39sd.cn/202203071)

### [15.zk 的命名服务（文件系统）](http://39sd.cn/202203071)

### [16.zk 的配置管理（文件系统、通知机制）](http://39sd.cn/202203071)

### [17.Zookeeper 集群管理（文件系统、通知机制）](http://39sd.cn/202203071)

### [18.Zookeeper 分布式锁（文件系统、通知机制）](http://39sd.cn/202203071)

### [19.获取分布式锁的流程](http://39sd.cn/202203071)

### [20.zookeeper 是如何保证事务的顺序一致性的？](http://39sd.cn/202203071)

### [21.Zookeeper 下 Server 工作状态](http://39sd.cn/202203071)

### [22.zookeeper 是如何选取主 leader 的？](http://39sd.cn/202203071)

### [23.zookeeper watch 机制](http://39sd.cn/202203071)

### **下载链接**：[**博主已将以上这些面试题整理成了一个面试手册，是PDF版的**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)