**Java常见面试题及答案**，每道都是认真筛选出的高频面试题，助力大家能找到满意的工作！

### **下载链接**：[**全部面试题及答案PDF**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)

### [1、什么是 Class 文件？ Class 文件主要的信息结构有哪些？](http://39sd.cn/202203251)

Class 文件是一组以 8 位字节为基础单位的二进制流。各个数据项严格按顺序排列。

Class 文件格式采用一种类似于 C 语言结构体的伪结构来存储数据。这样的伪结构仅仅有两种数据类型：无符号数和表。

无符号数：是基本数据类型。以 u1、u2、u4、u8 分别代表 1 个字节、2 个字节、4 个字节、8 个字节的无符号数，能够用来描写叙述数字、索引引用、数量值或者依照 UTF-8 编码构成的字符串值。

表：由多个无符号数或者其它表作为数据项构成的复合数据类型。全部表都习惯性地以 _info 结尾。

### [2、并发编程三个必要因素是什么？](http://39sd.cn/202203251)

原子性：

原子，即一个不可再被分割的颗粒。原子性指的是一个或多个操作要么全部执行成功要么全部执行失败。

可见性：

一个线程对共享变量的修改,另一个线程能够立刻看到。（synchronized,volatile）

有序性：

程序执行的顺序按照代码的先后顺序执行。（处理器可能会对指令进行重排序）

### [3、说下有哪些类加载器？](http://39sd.cn/202203251)

Bootstrap ClassLoader（启动类加载器） Extention ClassLoader（扩展类加载器） App ClassLoader（应用类加载器）

### [4、线程的调度策略](http://39sd.cn/202203251)

线程调度器选择优先级最高的线程运行，但是，如果发生以下情况，就会终止线程的运行：

1、 线程体中调用了yield方法让出了对cpu的占用权利

2、 线程体中调用了sleep方法使线程进入睡眠状态

3、 线程由于IO操作受到阻塞

4、 另外一个更高优先级线程出现

5）在支持时间片的系统中，该线程的时间片用完

### [5、用Java写一个冒泡排序。](http://39sd.cn/202203251)

冒泡排序几乎是个程序员都写得出来，但是面试的时候如何写一个逼格高的冒泡排序却不是每个人都能做到，下面提供一个参考代码：

```
public class BubbleSortTest {

　　public static void main(String[] args) {

　　int[] arr = new int[]{-12,3,2,34,5,8,1};
　　//冒泡排序
　　for(int i = 0;i < arr.length-1;i++){
　　　　for(int j = 0;j <arr.length-1-i;j++){
　　　　　　if(arr[j] >arr[j+1]){
　　　　　　int temp = arr[j];
　　　　　　arr[j] = arr[j+1];
　　　　　　arr[j+1] = temp;
　　　　　　}
　　　　}
　　}
　　//遍历
　　for (int i = 0; i < arr.length; i++) {
　　　　System.out.println(arr[i]+"\t");
　　}

　　}
}
```

### [6、Java对象的布局了解过吗？](http://39sd.cn/202203251)

对象头区域此处存储的信息包括两部分：1、对象自身的运行时数据( MarkWord )，占8字节 存储 hashCode、GC 分代年龄、锁类型标记、偏向锁线程 ID 、 CAS 锁指向线程 LockRecord 的指针等， synconized 锁的机制与这个部分( markwork )密切相关，用 markword 中最低的三位代表锁的状态，其中一位是偏向锁位，另外两位是普通锁位。2、对象类型指针( Class Pointer )，占4字节 对象指向它的类元数据的指针、 JVM 就是通过它来确定是哪个 Class 的实例。

实例数据区域 此处存储的是对象真正有效的信息，比如对象中所有字段的内容

对齐填充区域 JVM 的实现 HostSpot 规定对象的起始地址必须是 8 字节的整数倍，换句话来说，现在 64 位的 OS 往外读取数据的时候一次性读取 64bit 整数倍的数据，也就是 8 个字节，所以 HotSpot 为了高效读取对象，就做了"对齐"，如果一个对象实际占的内存大小不是 8byte 的整数倍时，就"补位"到 8byte 的整数倍。所以对齐填充区域的大小不是固定的。

### [7、解释什么是Tomcat Valve?](http://39sd.cn/202203251)

Tomcat Valve——Tomcat 4引入的新技术，它允许您将Java类的实例链接到一个特定的Catalina容器。

### [8、同步方法和同步块，哪个是更好的选择？](http://39sd.cn/202203251)

同步块是更好的选择，因为它不会锁住整个对象（当然你也可以让它锁住整个对象）。同步方法会锁住整个对象，哪怕这个类中有多个不相关联的同步块，这通常会导致他们停止执行并需要等待获得这个对象上的锁。

同步块更要符合开放调用的原则，只在需要锁住的代码块锁住相应的对象，这样从侧面来说也可以避免死锁。

### [9、运行时常量池的作用是什么?](http://39sd.cn/202203251)

运行时常量池是方法区的一部分，Class 文件中除了有类的版本、字段、方法、接口等描述信息外，还有一项信息是常量池表，用于存放编译器生成的各种字面量与符号引用，这部分内容在类加载后存放到运行时常量池。一般除了保存 Class 文件中描述的符号引用外，还会把符号引用翻译的直接引用也存储在运行时常量池。

运行时常量池相对于 Class 文件常量池的一个重要特征是动态性，Java 不要求常量只有编译期才能产生，运行期间也可以将新的常量放入池中，这种特性利用较多的是 String 的 intern 方法。

运行时常量池是方法区的一部分，受到方法区内存的限制，当常量池无法再申请到内存时会抛出 OutOfMemoryError。

### [10、如果你提交任务时，线程池队列已满，这时会发生什么](http://39sd.cn/202203251)

有俩种可能：

1、 如果使用的是无界队列 LinkedBlockingQueue，也就是无界队列的话，没关系，继续添加任务到阻塞队列中等待执行，因为 LinkedBlockingQueue 可以近乎认为是一个无穷大的队列，可以无限存放任务

2、 如果使用的是有界队列比如 ArrayBlockingQueue，任务首先会被添加到ArrayBlockingQueue 中，ArrayBlockingQueue 满了，会根据maximumPoolSize 的值增加线程数量，如果增加了线程数量还是处理不过来，ArrayBlockingQueue 继续满，那么则会使用拒绝策略RejectedExecutionHandler 处理满了的任务，默认是 AbortPolicy

### [11、如何写一段简单的死锁代码？](http://39sd.cn/202203251)

这个笔试的话频率也挺高（遇见笔试的公司要三思啊），所以这里直接给出一个答案（有很多版本的）。

### [12、栈帧都有哪些数据？](http://39sd.cn/202203251)

JVM的运行是基于栈的，和C语言的栈类似，它的大多数数据都是在堆里面的，只有少部分运行时的数据存在于栈上。

在JVM中，每个线程栈里面的元素，就叫栈帧。

栈帧包含：局部变量表、操作数栈、动态连接、返回地址等。

### [13、float f=3.4;是否正确？](http://39sd.cn/202203251)

答:不正确。3.4是双精度数，将双精度型（double）赋值给浮点型（float）属于下转型（down-casting，也称为窄化）会造成精度损失，因此需要强制类型转换float f =(float)3.4; 或者写成float f =3.4F;。

### [14、形参与实参](http://39sd.cn/202203251)

形参：全称为“形式参数”，是在定义方法名和方法体的时候使用的参数，用于接收调用该方法时传入的实际值；实参：全称为“实际参数”，是在调用方法时传递给该方法的实际值。

### [15、BIO、NIO、AIO 有什么区别？](http://39sd.cn/202203251)

BIO：Block IO 同步阻塞式 IO，就是我们平常使用的传统 IO，它的特点是模式简单使用方便，并发处理能力低。

NIO：New IO 同步非阻塞 IO，是传统 IO 的升级，客户端和服务器端通过 Channel（通道）通讯，实现了多路复用。

AIO：Asynchronous IO 是 NIO 的升级，也叫 NIO2，实现了异步非堵塞 IO ，异步 IO 的操作基于事件和回调机制。

### [16、什么是自旋](http://39sd.cn/202203251)

很多synchronized里面的代码只是一些很简单的代码，执行时间非常快，此时等待的线程都加锁可能是一种不太值得的操作，因为线程阻塞涉及到用户态和内核态切换的问题。既然synchronized里面的代码执行得非常快，不妨让等待锁的线程不要被阻塞，而是在synchronized的边界做忙循环，这就是自旋。如果做了多次忙循环发现还没有获得锁，再阻塞，这样可能是一种更好的策略。

### [17、我们可以在 hashcode() 中使用随机数字吗？](http://39sd.cn/202203251)

答案

http://javarevisited.blogspot.sg/2011/10/override-hashcode-in-java-example.html

不行，因为对象的 hashcode 值必须是相同的。参见答案获取更多关于 Java 中重写 hashCode() 方法的知识。

### [18、你所知道网络协议有那些？](http://39sd.cn/202203251)

1、 HTTP：超文本传输协议

2、 FTP：文件传输协议

3、 SMPT：简单邮件协议

4、 TELNET：远程终端协议

5、 POP3：邮件读取协议

### [19、构造器Constructor是否可被override](http://39sd.cn/202203251)

在讲继承的时候我们就知道父类的私有属性和构造方法并不能被继承，所以Constructor也就不能被override,但是可以overload,所以你可以看到一个类中有多个构造函数的情况。

### [20、什么是双亲委派机制？](http://39sd.cn/202203251)

双亲委派机制的意思是除了顶层的启动类加载器以外，其余的类加载器，在加载之前，都会委派给它的父加载器进行加载。这样一层层向上传递，直到祖先们都无法胜任，它才会真正的加载。

### [21、讲讲什么情况下会出现内存溢出，内存泄漏？](http://39sd.cn/202203251)

### [22、乐观锁和悲观锁的理解及如何实现，有哪些实现方式？](http://39sd.cn/202203251)

### [23、线程与进程的区别？](http://39sd.cn/202203251)

### [24、Session的save()、update()、merge()、lock()、saveOrUpdate()和persist()方法分别是做什么的？有什么区别？](http://39sd.cn/202203251)

### [25、用代码演示三种代理](http://39sd.cn/202203251)

### [27、分代收集算法](http://39sd.cn/202203251)

### [28、同步方法和同步块，哪个是更好的选择？](http://39sd.cn/202203251)

### [29、Java 中的编译期常量是什么？使用它又什么风险？](http://39sd.cn/202203251)

### [30、Java死锁以及如何避免？](http://39sd.cn/202203251)

### [31、什么是“依赖注入”和“控制反转”？为什么有人使用？](http://39sd.cn/202203251)

### [32、ArrayList 和 LinkedList 的区别是什么？](http://39sd.cn/202203251)

### [33、观察者模式应用场景](http://39sd.cn/202203251)

### [34、Array与ArrayList有什么不一样？](http://39sd.cn/202203251)

### [35、实例化数组后，能不能改变数组长度呢？](http://39sd.cn/202203251)

### [36、Java 中，Maven 和 ANT 有什么区别？](http://39sd.cn/202203251)

### [37、方法区的作用是什么？](http://39sd.cn/202203251)

### [38、接口和抽象类有什么区别？](http://39sd.cn/202203251)

### [39、原型模式的应用场景](http://39sd.cn/202203251)

### [40、ConcurrentHashMap 和 Hashtable 的区别？](http://39sd.cn/202203251)

### **下载链接**：[**博主已将以上这些面试题整理成了一个面试手册，是PDF版的**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)