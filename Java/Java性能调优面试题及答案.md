
**Java性能调优面试题及答案**，每道都是认真筛选出的高频面试题，助力大家能找到满意的工作！

### **下载链接**：[**全部面试题及答案PDF**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)

## 常用的性能评价/测试指标

### [1.相应时间](http://39sd.cn/202202221)

提交请求和返回该请求的响应之间使用的时间，一般比较关注平均响应时间。

常用操作的响应时间列表：

|  操作   | 响应时间  |
|  ----  | ----  |
| 打开一个网站  | 几秒 |
| 数据库查询一条记录  | 十几毫秒 |
| 机械磁盘一次寻址定位  | 4 毫秒 |
| 从机械磁盘顺序读取 1M 数据  | 2 毫秒 |
| 从 SSD 磁盘顺序读取 1M 数据  | 0.3 毫秒 |
| 从远程分布式换成 Redis 读取一个数据  | 0.5 毫秒 |
| 从内存读取 1M 数据  | 十几微妙 |
| Java 程序本地方法调用  | 几微妙 |
| 网络传输 2Kb 数据  | 1 微妙 |

### [2.并发数](http://39sd.cn/202202221)

同一时刻，对服务器有实际交互的请求数。

和网站在线用户数的关联：1000 个同时在线用户数，可以估计并发数在 5%到 15%之间，

也就是同时并发数在 50~150 之间。

### [3.吞吐量](http://39sd.cn/202202221)

对单位时间内完成的工作量(请求)的量度。

### [4.关系](http://39sd.cn/202202221)

系统吞吐量和系统并发数以及响应时间的关系：

理解为高速公路的通行状况：

吞吐量是每天通过收费站的车辆数目（可以换算成收费站收取的高速费），并发数是高速公路上的正在行驶的车辆数目，响应时间是车速。

车辆很少时，车速很快。但是收到的高速费也相应较少；随着高速公路上车辆数目的增
多，车速略受影响，但是收到的高速费增加很快；

随着车辆的继续增加，车速变得越来越慢，高速公路越来越堵，收费不增反降；

如果车流量继续增加，超过某个极限后，任务偶然因素都会导致高速全部瘫痪，车走不动，当然后也收不着，而高速公路成了停车场（资源耗尽）。

## 常用的性能优化手段

### [5.避免过早优化](http://39sd.cn/202202221)

不应该把大量的时间耗费在小的性能改进上，过早考虑优化是所有噩梦的根源。

所以，我们应该编写清晰，直接，易读和易理解的代码，真正的优化应该留到以后，等到性能分析表明优化措施有巨大的收益时再进行。

但是过早优化，不表示我们应该编写已经知道的对性能不好的的代码结构。如《4、编写高效优雅 Java 程序》中的《15、当心字符串连接的性能》所说的部分。

### [6.进行系统性能测试](http://39sd.cn/202202221)

所有的性能调优，都有应该建立在性能测试的基础上，直觉很重要，但是要用数据说话，可以推测，但是要通过测试求证。

### [7.寻找系统瓶颈，分而治之，逐步优化](http://39sd.cn/202202221)

性能测试后，对整个请求经历的各个环节进行分析，排查出现性能瓶颈的地方，定位问题，分析影响性能的的主要因素是什么？内存、磁盘 IO、网络、CPU，还是代码问题？架构设计不足？或者确实是系统资源不足？

## 前端优化常用手段

### [8.减少请求数](http://39sd.cn/202202221)

合并 CSS，Js，图片

### [9.使用客户端缓冲](http://39sd.cn/202202221)

静态资源文件缓存在浏览器中，有关的属性 Cache-Control 和 Expires
如果文件发生了变化，需要更新，则通过改变文件名来解决。

### [10.启用压缩](http://39sd.cn/202202221)

减少网络传输量，但会给浏览器和服务器带来性能的压力，需要权衡使用。

### [11.资源文件加载顺序](http://39sd.cn/202202221)

css 放在页面最上面，js 放在最下面

### [12.减少 Cookie 传输](http://39sd.cn/202202221)

cookie 包含在每次的请求和响应中，因此哪些数据写入 cookie 需要慎重考虑

### [13.给用户一个提示](http://39sd.cn/202202221)

有时候在前端给用户一个提示，就能收到良好的效果。毕竟用户需要的是不要不理他。

### [14.CDN 加速](http://39sd.cn/202202221)

CDN，又称内容分发网络，本质仍然是一个缓存，而且是将数据缓存在用户最近的地方。

无法自行实现 CDN 的时候，可以考虑商用 CDN 服务。

### [15.反向代理缓存](http://39sd.cn/202202221)

将静态资源文件缓存在反向代理服务器上，一般是 Nginx。

### [16.WEB 组件分离](http://39sd.cn/202202221)

## 应用服务性能优化

### [17.缓存](http://39sd.cn/202202221)

### [18.缓存的基本原理和本质](http://39sd.cn/202202221)

### [19.合理使用缓冲的准则](http://39sd.cn/202202221)

### [20.分布式缓存与一致性哈希](http://39sd.cn/202202221)

## 异步

### [21.同步和异步，阻塞和非阻塞](http://39sd.cn/202202221)

### [22.常见的异步手段](http://39sd.cn/202202221)

### [23.集群优势](http://39sd.cn/202202221)

## 程序

### [24.代码级别调优](http://39sd.cn/202202221)

### [25.并发编程调优](http://39sd.cn/202202221)

### [26.资源复用调优](http://39sd.cn/202202221)

## JVM调优

### [27.与 JIT 编译器相关的优化](http://39sd.cn/202202221)

### [28.热点编译的概念](http://39sd.cn/202202221)

### [29.选择编译器类型](http://39sd.cn/202202221)

### [30.代码缓存相关](http://39sd.cn/202202221)

### [31.编译阈值](http://39sd.cn/202202221)

### [32.编译线程](http://39sd.cn/202202221)

### [33.方法内联](http://39sd.cn/202202221)

### [34.逃逸分析](http://39sd.cn/202202221)

### 全部面试题及答案已整理好！！！！

### **下载链接**：[**博主已将以上这些面试题整理成了一个面试手册，是PDF版的**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)