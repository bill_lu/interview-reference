**Kafka面试题及答案**，每道都是认真筛选出的高频面试题，助力大家能找到满意的工作！

### **下载链接**：[**全部面试题及答案PDF**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)

### [1. kafka 可以脱离 zookeeper 单独使用吗？为什么？](http://39sd.cn/202203012)

kafka 不能脱离 zookeeper 单独使用，因为 kafka 使用 zookeeper 管理和协调 kafka 的节点服务器。

### [2. kafka 有几种数据保留的策略？](http://39sd.cn/202203012)

kafka 有两种数据保存策略：按照过期时间保留和按照存储的消息大小保留。

### [3. kafka 同时设置了 7 天和 10G 清除数据，到第五天的时候消息达到了 10G，这个时候 kafka 将如何处理？](http://39sd.cn/202203012)

这个时候 kafka 会执行数据清除工作，时间和大小不论那个满足条件，都会清空数据。

### [4. 什么情况会导致 kafka 运行变慢？](http://39sd.cn/202203012)

•	cpu 性能瓶颈

•	磁盘读写瓶颈

•	网络瓶颈

### [5. 使用 kafka 集群需要注意什么？](http://39sd.cn/202203012)

•	集群的数量不是越多越好，最好不要超过 7 个，因为节点越多，消息复制需要的时间就越长，整个群组的吞吐量就越低。

•	集群数量最好是单数，因为超过一半故障集群就不能用了，设置为单数容错率更高。

### [6.Kafka 的设计时什么样的呢？](http://39sd.cn/202203012)

Kafka 将消息以 topic 为单位进行归纳

将向 Kafka topic 发布消息的程序成为 producers.

将预订 topics 并消费消息的程序成为 consumer.

Kafka 以集群的方式运行，可以由一个或多个服务组成，每个服务叫做一个 broker.

producers 通过网络将消息发送到 Kafka 集群，集群向消费者提供消息

### [7.数据传输的事物定义有哪三种？](http://39sd.cn/202203012)

数据传输的事务定义通常有以下三种级别：

（1）最多一次: 消息不会被重复发送，最多被传输一次，但也有可能一次不传输

（2）最少一次: 消息不会被漏发送，最少被传输一次，但也有可能被重复传输. 

（3）精确的一次（Exactly once）: 不会漏传输也不会重复传输,每个消息都传输被一次而且
仅仅被传输一次，这是大家所期望的

### [8.Kafka 判断一个节点是否还活着有那两个条件？](http://39sd.cn/202203012)

（1）节点必须可以维护和 ZooKeeper 的连接，Zookeeper 通过心跳机制检查每个节点的连接

（2）如果节点是个 follower,他必须能及时的同步 leader 的写操作，延时不能太久

### [9.producer 是否直接将数据发送到 broker 的 leader(主节点)？](http://39sd.cn/202203012)

producer 直接将数据发送到 broker 的 leader(主节点)，不需要在多个节点进行分发，为了帮
助 producer 做到这点，所有的 Kafka 节点都可以及时的告知:哪些节点是活动的，目标 topic
目标分区的 leader 在哪。这样 producer 就可以直接将消息发送到目的地了

### [10.Kafka consumer 是否可以消费指定分区消息？](http://39sd.cn/202203012)

Kafa consumer 消费消息时，向 broker 发出"fetch"请求去消费特定分区的消息，consumer 指定消息在日志中的偏移量（offset），就可以消费从这个位置开始的消息，customer 拥有了
offset 的控制权，可以向后回滚去重新消费之前的消息，这是很有意义的

### [11.Kafka 消息是采用 Pull 模式，还是 Push 模式？](http://39sd.cn/202203012)

Kafka 最初考虑的问题是，customer 应该从 brokes 拉取消息还是 brokers 将消息推送到
consumer，也就是 pull 还 push。在这方面，Kafka 遵循了一种大部分消息系统共同的传统的
设计：producer 将消息推送到 broker，consumer 从 broker 拉取消息

一些消息系统比如 Scribe 和 Apache Flume 采用了 push 模式，将消息推送到下游的 consumer。
这样做有好处也有坏处：由 broker 决定消息推送的速率，对于不同消费速率的 consumer 就
不太好处理了。消息系统都致力于让 consumer 以最大的速率最快速的消费消息，但不幸的
是，push 模式下，当 broker 推送的速率远大于 consumer 消费的速率时，consumer 恐怕就
要崩溃了。最终 Kafka 还是选取了传统的 pull 模式

Pull 模式的另外一个好处是 consumer 可以自主决定是否批量的从 broker 拉取数据。Push 模
式必须在不知道下游 consumer 消费能力和消费策略的情况下决定是立即推送每条消息还是
缓存之后批量推送。如果为了避免 consumer 崩溃而采用较低的推送速率，将可能导致一次
只推送较少的消息而造成浪费。Pull 模式下，consumer 就可以根据自己的消费能力去决定这
些策略

Pull 有个缺点是，如果 broker 没有可供消费的消息，将导致 consumer 不断在循环中轮询，
直到新消息到 t 达。为了避免这点，Kafka 有个参数可以让 consumer 阻塞知道新消息到达(当
然也可以阻塞知道消息的数量达到某个特定的量这样就可以批量发

### [12.Kafka 存储在硬盘上的消息格式是什么？](http://39sd.cn/202203012)

消息由一个固定长度的头部和可变长度的字节数组组成。头部包含了一个版本号和 CRC32
校验码。

消息长度: 4 bytes (value: 1+4+n)

版本号: 1 byte

CRC校验码: 4 bytes

具体的消息: n bytes

### [13.Kafka 高效文件存储设计特点：](http://39sd.cn/202203012)

### [14.Kafka 与传统消息系统之间有三个关键区别](http://39sd.cn/202203012)

### [15.partition 的数据如何保存到硬盘](http://39sd.cn/202203012)

### [16.kafka 的 ack 机制](http://39sd.cn/202203012)

### [17.Kafka 的消费者如何消费数据](http://39sd.cn/202203012)

### [18.消费者负载均衡策略](http://39sd.cn/202203012)

### [19.数据有序](http://39sd.cn/202203012)

### [20.kafaka 生产数据时数据的分组策略](http://39sd.cn/202203012)

### [21.Kafka 创建 Topic 时如何将分区放置到不同的 Broker中](http://39sd.cn/202203012)

### [22.Kafka 新建的分区会在哪个目录下创建](http://39sd.cn/202203012)

### **下载链接**：[**博主已将以上这些面试题整理成了一个面试手册，是PDF版的**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)