**MySQL经典面试题及答案**，每道都是认真筛选出的大厂高频面试题，助力大家能找到满意的工作！

### **下载链接**：[**全部MySQL面试题及答案PDF**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)


### [1.Mysql 服务器默认端口是什么？ ](http://39sd.cn/201201041)

Mysql 服务器的默认端口是 3306。

### [2.与 Oracle 相比，Mysql 有什么优势？](http://39sd.cn/201201041)

Mysql 是开源软件，随时可用，无需付费。
 
Mysql 是便携式的 

带有命令提示符的 GUI

使用 Mysql 查询浏览器支持管理

### [3.常用的通用 SQL 函数？](http://39sd.cn/201201041)

- CONCAT(A, B) - 连接两个字符串值以创建单个字符串输出。通常用于将两个或多个字段合并为一个字段。
- FORMAT(X, D)- 格式化数字 X 到 D 有效数字。
- CURRDATE(), CURRTIME()- 返回当前日期或时间。
- NOW（） - 将当前日期和时间作为一个值返回。
MONTH（），DAY（），YEAR（），WEEK（），WEEKDAY（） - 从日期值中提取给定数据。
- HOUR（），MINUTE（），SECOND（） - 从时间值中提取给定数据。
- DATEDIFF（A，B） - 确定两个日期之间的差异，通常用于计算年龄
- SUBTIMES（A，B） - 确定两次之间的差异。
- FROMDAYS（INT） - 将整数天数转换为日期值。

### [4.CHAR 和 VARCHAR 的区别？](http://39sd.cn/201201041) 

以下是 CHAR 和 VARCHAR 的区别： 

CHAR 和 VARCHAR 类型在存储和检索方面有所不同 

CHAR 列长度固定为创建表时声明的长度，长度值范围是 1 到 255 

当 CHAR 值被存储时，它们被用空格填充到特定长度，检索 CHAR 值时需删除尾随空格。

### [5.列的字符串类型可以是什么？](http://39sd.cn/201201041)

字符串类型是：

- SET 
- BLOB 
- ENUM 
- CHAR 
- TEXT 
- VARCHAR

### [6.如何获取当前的 Mysql 版本？](http://39sd.cn/201201041)

SELECT VERSION();用于获取当前 Mysql 的版本。

### [7.Mysql 中使用什么存储引擎？](http://39sd.cn/201201041)

存储引擎称为表类型，数据使用各种技术存储在文件中。 
技术涉及： 

- Storage mechanism 
- Locking levels 
- Indexing 
- Capabilities and functions.

### [8.我们如何得到受查询影响的行数？](http://39sd.cn/201201041)

行数可以通过以下代码获得： SELECT COUNT(user_id)FROM users;

### [9.Mysql 查询是否区分大小写？](http://39sd.cn/201201041)

不区分 

SELECT VERSION(), CURRENT_DATE; 

SeLect version(), current_date; 

seleCt vErSiOn(), current_DATE; 

所有这些例子都是一样的，Mysql 不区分大小写。

### [10.主键和候选键有什么区别？](http://39sd.cn/201201041)

表格的每一行都由主键唯一标识,一个表只有一个主键。

主键也是候选键。按照惯例，候选键可以被指定为主键，并且可以用于任何外键引用。

### [11.MYSQL 数据表在什么情况下容易损坏？](http://39sd.cn/201201041)

服务器突然断电导致数据文件损坏。

强制关机，没有先关闭 mysql 服务等。

### [12.mysql 有关权限的表都有哪几个？](http://39sd.cn/201201041)

Mysql 服务器通过权限表来控制用户对数据库的访问，权限表存放在 mysql 数据库里，由mysql_install_db 脚本初始化。

这些权限表分别 user，db，table_priv，columns_priv 和 host。

### [13.Mysql 中有哪几种锁？](http://39sd.cn/201201041)

MyISAM 支持表锁，InnoDB 支持表锁和行锁，默认为行锁

表级锁：开销小，加锁快，不会出现死锁。锁定粒度大，发生锁冲突的概率最高，并发量最低

行级锁：开销大，加锁慢，会出现死锁。锁力度小，发生锁冲突的概率小，并发度最高

### [14.LIKE 和 REGEXP 操作有什么区别？](http://39sd.cn/201201041)

LIKE 和 REGEXP 运算符用于表示^和％。

SELECT * FROM employee WHERE emp_name REGEXP "^b"; 

SELECT * FROM employee WHERE emp_name LIKE "%b";

### [15.如何看到为表格定义的所有索引？](http://39sd.cn/201201041)

索引是通过以下方式为表格定义的： SHOW INDEX FROM <tablename>;

### [16.BLOB 和 TEXT 有什么区别？](http://39sd.cn/201201041)

BLOB 是一个二进制对象，可以容纳可变数量的数据。有四种类型的 BLOB -

- TINYBLOB
- BLOB
- MEDIUMBLOB
- LONGBLOB

它们只能在所能容纳价值的最大长度上有所不同。

TEXT 是一个不区分大小写的 BLOB。四种 TEXT 类型

- TINYTEXT
- TEXT
- MEDIUMTEXT
- LONGTEXT

它们对应于四种 BLOB 类型，并具有相同的最大长度和存储要求。

BLOB 和 TEXT 类型之间的唯一区别在于对 BLOB 值进行排序和比较时区分大小写，对TEXT 值不区分大小写。

### [17.LIKE 声明中的％和_是什么意思？](http://39sd.cn/201201041)

### [18.如何在 Unix 和 Mysql 时间戳之间进行转换？](http://39sd.cn/201201041)

### [19.我们如何在 mysql 中运行批处理模式？](http://39sd.cn/201201041)

### [20.MYSQL 支持事务吗？](http://39sd.cn/201201041)

### [21.mysql_fetch_array 和 mysql_fetch_object 的区别是什么？](http://39sd.cn/201201041)

### [22.Mysql 中有哪些不同的表格？](http://39sd.cn/201201041)

### [23.Mysql 表中允许有多少个 TRIGGERS？](http://39sd.cn/201201041)

### [24.InnoDB 是什么？](http://39sd.cn/201201041)

### [25.ISAM 是什么？](http://39sd.cn/201201041)

### [26.Mysql 的技术特点是什么？](http://39sd.cn/201201041)

### [27.Heap 表是什么？](http://39sd.cn/201201041)

### [28.如何显示前 50 行？](http://39sd.cn/201201041)

### [29.如何区分 FLOAT 和 DOUBLE？](http://39sd.cn/201201041)

### [30.如何输入字符为十六进制数字？](http://39sd.cn/201201041)

### [31.可以使用多少列创建索引？](http://39sd.cn/201201041)

### [32.NOW（）和CURRENT_DATE（）有什么区别？](http://39sd.cn/201201041)

### [33.区分 CHAR_LENGTH 和 LENGTH？](http://39sd.cn/201201041)

### [34.什么样的对象可以使用 CREATE 语句创建？](http://39sd.cn/201201041)

### [35.列对比运算符是什么？](http://39sd.cn/201201041)

### [36.什么是非标准字符串类型？](http://39sd.cn/201201041)

### [37.Mysql 如何优化 DISTINCT？](http://39sd.cn/201201041)

### [38.如何控制 HEAP 表的最大尺寸？](http://39sd.cn/201201041)

### [39.一张表，里面有 ID 自增主键，当 insert 了 17 条记录之后， 删除了第 15,16,17 条记录，再把 Mysql 重启，再 insert 一条记录，这条记录的ID 是 18 还是 15 ？](http://39sd.cn/201201041)

### [40.在 Mysql 中 ENUM 的用法是什么？](http://39sd.cn/201201041)

### [41.如何定义 REGEXP？](http://39sd.cn/201201041)

### [42.MyISAM Static 和 MyISAM Dynamic 有什么区别？](http://39sd.cn/201201041)

### [43.federated 表是什么？](http://39sd.cn/201201041)

### [44.如果一个表有一列定义为 TIMESTAMP，将发生什么？](http://39sd.cn/201201041)

### [45.列设置为 AUTO INCREMENT 时，如果在表中达到最大值，会发生什么情况？](http://39sd.cn/201201041)

### [46.怎样才能找出最后一次插入时分配了哪个自动增量？](http://39sd.cn/201201041)

### 全部MySQL面试题及答案已整理好！！！！


### **下载链接**：[**博主已将以上这些大厂面试题整理成了一个面试手册，是PDF版的**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)