**Dubbo面试题及答案**，每道都是认真筛选出的高频面试题，助力大家能找到满意的工作！

### **下载链接**：[**全部面试题及答案PDF**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)

### [1、Dubbo是什么？](http://39sd.cn/202203241)

Dubbo是阿里巴巴开源的基于 Java 的高性能 RPC 分布式服务框架，现已成为 Apache 基金会孵化项目。

面试官问你如果这个都不清楚，那下面的就没必要问了。

官网：http://dubbo.apache.org

### [2、为什么要用Dubbo？](http://39sd.cn/202203241)

因为是阿里开源项目，国内很多互联网公司都在用，已经经过很多线上考验。内部使用了 Netty、Zookeeper，保证了高性能高可用性。

使用 Dubbo 可以将核心业务抽取出来，作为独立的服务，逐渐形成稳定的服务中心，可用于提高业务复用灵活扩展，使前端应用能更快速的响应多变的市场需求。

### [3、Dubbo超时时间怎样设置？](http://39sd.cn/202203241)

1） 服务提供者端设置超时时间，在 Dubbo 的用户文档中，推荐如果能在服务端多配置就尽量多配置，因为服务提供者比消费者更清楚自己提供的服务特性。

2）服务消费者端设置超时时间，如果在消费者端设置了超时时间，以消费者端为主，即优先级更高。因为服务调用方设置超时时间控制性更灵活。如果消费方超时，服务端线程不会定制，会产生警告。

### [4、Dubbo与Spring的关系](http://39sd.cn/202203241)

Dubbo 采用全 Spring 配置方式，透明化接入应用，对应用没有任何API 侵入，只需用 Spring 加载 Dubbo 的配置即可，Dubbo 基于Spring 的 Schema 扩展进行加载。

### [5、Dubbo 和 Spring Cloud 有什么区别？](http://39sd.cn/202203241)

两个没关联，如果硬要说区别，有以下几点。

1）通信方式不同

Dubbo 使用的是 RPC 通信，而 Spring Cloud 使用的是 HTTP RESTFul 方式。

2）组成部分不同


|  组件   | Dubbo  | Spring Clond  |
|  ----  | ----  | ---- |
| 服务注册中心  | Zookeeper | Spring Cloud Netflix Eureka
| 服务监控  | Dubbo-monitor | Spring Boot Admin
| 断路器  | 不完善 | Spring Cloud Netflix Hystrix
| 服务网关  | 无 | Spring Cloud Netflix Gateway
| 分布式配置  | 无 | Spring Cloud Config
| 服务跟踪  | 无 | Spring Cloud Sleuth
| 消息总线 | 无 | Spring Cloud Bus
| 数据流   | 无 | Spring Cloud Stream
| 批量任务  | 无 | Spring Cloud Task


### [6、Dubbo默认使用的是什么通信框架，还有别的选择吗？](http://39sd.cn/202203241)

Dubbo 默认使用 Netty 框架，也是推荐的选择，另外内容还集成有Mina、Grizzly。

### [7、服务上线怎么兼容旧版本？](http://39sd.cn/202203241)

可以用版本号（version）过渡，多个不同版本的服务注册到注册中心，版本号不同的服务相互间不引用。这个和服务分组的概念有一点类似。

### [8、Dubbo可以对结果进行缓存吗？](http://39sd.cn/202203241)

可以，Dubbo 提供了声明式缓存，用于加速热门数据的访问速度，以减少用户加缓存的工作量。

### [9、Dubbo服务之间的调用是阻塞的吗？](http://39sd.cn/202203241)

默认是同步等待结果阻塞的，支持异步调用。

Dubbo 是基于 NIO 的非阻塞实现并行调用，客户端不需要启动多线程即可完成并行调用多个远程服务，相对多线程开销较小，异步调用会返回一个 Future 对象。

### [10、Dubbo支持分布式事务吗？](http://39sd.cn/202203241)

目前暂时不支持，后续可能采用基于 JTA/XA 规范实现。

### [11、Dubbo telnet 命令能做什么？](http://39sd.cn/202203241)

dubbo 通过 telnet 命令来进行服务治理

telnet localhost 8090

### [12、Dubbo 停止维护了吗？](http://39sd.cn/202203241)

2014 年开始停止维护过几年，17 年开始重新维护，并进入了 Apache 项目。

### [13、Dubbo 和 Dubbox 有什么区别？](http://39sd.cn/202203241)

Dubbox 是继 Dubbo 停止维护后，当当网基于 Dubbo 做的一个扩展项目，如加了服务可 Restful 调用，更新了开源组件等。

### [14、你还了解别的分布式框架吗？](http://39sd.cn/202203241)

别的还有 Spring cloud、Facebook 的 Thrift、Twitter 的 Finagle 等。

### [15、Dubbo 能集成 Spring Boot 吗？](http://39sd.cn/202203241)

可以的，项目地址如下。

https://github.com/apache/incubator-dubbo-spring-boot-project

### [16、在使用过程中都遇到了些什么问题？](http://39sd.cn/202203241)

Dubbo 的设计目的是为了满足高并发小数据量的 rpc 调用，在大数据量下的性能表现并不好，建议使用 rmi 或 http 协议。

### [17、dubbo都支持什么协议，推荐用哪种？](http://39sd.cn/202203241)

dubbo://（推荐）

rmi://

hessian://

http://

webservice://

thrift://

memcached://

redis://

rest://

### [18、Dubbo需要 Web 容器吗？](http://39sd.cn/202203241)

不需要，如果硬要用 Web 容器，只会增加复杂性，也浪费资源。

### [19、Dubbo内置了哪几种服务容器？](http://39sd.cn/202203241)

Spring Container

Jetty Container

Log4j Container

Dubbo 的服务容器只是一个简单的 Main 方法，并加载一个简单的 Spring 容器，用于暴露服务。

### [20、Dubbo里面有哪几种节点角色？](http://39sd.cn/202203241)

|  节点   | 角色说明  |
|  ----  | ----  |
| Provider  | 暴露服务的服务提供方 |
| Consumer  | 调用远程服务的服务消费方 |
| Registry  | 服务注册与发现的注册中心 |
| Monitor  | 统计服务的调用次数和调用时间的监控中心 |
| Container  | 服务运行容器 |

### [21、画一画服务注册与发现的流程图](http://39sd.cn/202203241)

### [22、Dubbo默认使用什么注册中心，还有别的选择吗？](http://39sd.cn/202203241)

### [23、Dubbo有哪几种配置方式？](http://39sd.cn/202203241)

### [24、Dubbo 核心的配置有哪些？](http://39sd.cn/202203241)

### [25、在 Provider 上可以配置的 Consumer 端的属性有哪些？](http://39sd.cn/202203241)

### [26、Dubbo启动时如果依赖的服务不可用会怎样？](http://39sd.cn/202203241)

### [27、Dubbo推荐使用什么序列化框架，你知道的还有哪些？](http://39sd.cn/202203241)

### [28、Dubbo有哪几种集群容错方案，默认是哪种？](http://39sd.cn/202203241)

### [29、Dubbo有哪几种负载均衡策略，默认是哪种？](http://39sd.cn/202203241)

### [30、注册了多个同一样的服务，如果测试指定的某一个服务呢？](http://39sd.cn/202203241)

### [31、Dubbo支持服务多协议吗？](http://39sd.cn/202203241)

### [32、当一个服务接口有多种实现时怎么做？](http://39sd.cn/202203241)

### [33、Dubbo支持服务降级吗？](http://39sd.cn/202203241)

### [34、Dubbo如何优雅停机？](http://39sd.cn/202203241)

### [35、服务提供者能实现失效踢出是什么原理？](http://39sd.cn/202203241)

### [36、如何解决服务调用链过长的问题？](http://39sd.cn/202203241)

### [37、服务读写推荐的容错策略是怎样的？](http://39sd.cn/202203241)

### [38、Dubbo必须依赖的包有哪些？](http://39sd.cn/202203241)

### [39、Dubbo的管理控制台能做什么？](http://39sd.cn/202203241)

### [40、说说 Dubbo 服务暴露的过程](http://39sd.cn/202203241)

### [41、你读过 Dubbo 的源码吗？](http://39sd.cn/202203241)

### [42、你觉得用 Dubbo 好还是 Spring Cloud 好？](http://39sd.cn/202203241)

### 全部面试题及答案已整理好！！！！

### **下载链接**：[**博主已将以上这些面试题整理成了一个面试手册，是PDF版的**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)