**Spring经典面试题及答案**，每道都是认真筛选出的大厂高频面试题，助力大家能找到满意的工作！

### **下载链接**：[**全部面试题及答案PDF**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)

### [1.什么是 spring?](http://39sd.cn/201201042)

Spring 是个 java 企业级应用的开源开发框架。Spring 主要用来开发 Java 应用， 但是有些扩展是针对构建 J2EE 平台的 web 应用。Spring 框架目标是简化 Java 企业级应用开发，并通过 POJO 为基础的编程模型促进良好的编程习惯。

### [2.为什么要使用 spring？](http://39sd.cn/201201042)

•	spring 提供 ioc 技术，容器会帮你管理依赖的对象，从而不需要自己创建和管理依赖对象了，更轻松的实现了程序的解耦。

•	spring 提供了事务支持，使得事务操作变的更加方便。

•	spring 提供了面向切片编程，这样可以更方便的处理某一类的问题。

•	更方便的框架集成，spring 可以很方便的集成其他框架，比如 MyBatis、hibernate 等。

### [3.使用 Spring 框架的好处是什么？](http://39sd.cn/201201042)

轻量：Spring 是轻量的，基本的版本大约 2MB

控制反转：Spring 通过控制反转实现了松散耦合， 对象们给出它们的依赖， 而不是创建或查找依赖的对象们

面向切面的编程(AOP)：Spring 支持面向切面的编程，并且把应用业务逻辑和系统服务分开

容器：Spring 包含并管理应用中对象的生命周期和配置

MVC 框架： Spring 的 WEB 框架是个精心设计的框架， 是 Web 框架的一个很好的替代品

事务管理：Spring 提供一个持续的事务管理接口，可以扩展到上至本地事务下至全局事务（ JTA）

异常处理： Spring 提供方便的 API 把具体技术相关的异常（ 比如由 JDBC ， Hibernate or JDO 抛出的） 转化为一致的 unchecked 异常

### [4. 解释一下什么是 aop？](http://39sd.cn/201201042)

aop 是面向切面编程，通过预编译方式和运行期动态代理实现程序功能的统一维护的一种技术。

简单来说就是统一处理某一“切面”（类）的问题的编程思想，比如统一处理日志、异常等。

### [5. 解释一下什么是 ioc？](http://39sd.cn/201201042)

ioc：Inversionof Control（中文：控制反转）是 spring 的核心，对于 spring 框架来说，就是由 spring 来负责控制对象的生命周期和对象间的关系。

简单来说，控制指的是当前对象对内部成员的控制权；控制反转指的是，这种控制权不由当前对象管理了，由其他（类,第三方容器）来管理。

### [6.IOC 的优点是什么？](http://39sd.cn/201201042)

IOC 或 依赖注入把应用的代码量降到最低。它使应用容易测试，单元测试不再需要单例和 JNDI 查找机制。最小的代价和最小的侵入性使松散耦合得以实现。IOC 容器支持加载服务时的饿汉式初始化和懒加载。

### [7. spring 有哪些主要模块？](http://39sd.cn/201201042)

•	spring core：框架的最基础部分，提供 ioc 和依赖注入特性。

•	spring context：构建于 core 封装包基础上的 context 封装包，提供了一种框架式的对象访问方法。

•	spring dao：Data Access Object 提供了JDBC的抽象层。

•	spring aop：提供了面向切面的编程实现，让你可以自定义拦截器、切点等。
 
•	spring Web：提供了针对 Web 开发的集成特性，例如文件上传，利用 servlet listeners 进行 ioc 容器初始化和针对 Web 的 ApplicationContext。

•	spring Web mvc：spring 中的 mvc 封装包提供了 Web 应用的 Model-View-Controller（MVC）的实现。


### [8.BeanFactory – BeanFactory 实现举例](http://39sd.cn/201201042)

Bean 工厂是工厂模式的一个实现，提供了控制反转功能，用来把应用的配置和依赖从正真的应用代码中分离。

最常用的 BeanFactory 实现是 XmlBeanFactory 类。

### [9.XMLBeanFactory](http://39sd.cn/201201042)

最常用的就是 org.springframework.beans.factory.xml.XmlBeanFactory ，它根据 XML 文件中的定义加载 beans。该容器从 XML 文件读取配置元数据并用它去创建一个完全配置的系统或应用。

### [10.解释 AOP 模块](http://39sd.cn/201201042)

AOP 模块用于发给我们的 Spring 应用做面向切面的开发， 很多支持由 AOP 联盟提供， 这样就确保了 Spring 和其他 AOP 框架的共通性。这个模块将元数据编程引入Spring。

### [11.解释 JDBC 抽象和 DAO 模块](http://39sd.cn/201201042)

通过使用 JDBC 抽象和 DAO 模块，保证数据库代码的简洁，并能避免数据库资源错误关闭导致的问题，它在各种不同的数据库的错误信息之上，提供了一个统一的异常访问层。它还利用 Spring 的 AOP 模块给 Spring 应用中的对象提供事务管理服务。

### [12.什么是 Spring 的依赖注入？](http://39sd.cn/201201042)

依赖注入， 是 IOC 的一个方面， 是个通常的概念， 它有多种解释。这概念是说你不用创建对象，而只需要描述它如何被创建。你不在代码里直接组装你的组件和服务，   但是要在配置文件里描述哪些组件需要哪些服务，之后一个容器（ IOC 容器）负责把他们组装起来。

### [13.spring 常用的注入方式有哪些？](http://39sd.cn/201201042)

•	setter 属性注入

•	构造方法注入

•	注解方式注入

### [14.有哪些不同类型的 IOC（依赖注入）方式？](http://39sd.cn/201201042)

构造器依赖注入：构造器依赖注入通过容器触发一个类的构造器来实现的，该类有一   系列参数，每个参数代表一个对其他类的依赖。

Setter 方法注入：Setter 方法注入是容器通过调用无参构造器或无参 static 工厂 方法实例化 bean 之后， 调用该 bean 的 setter 方法， 即实现了基于 setter 的依赖注入。

### [15.哪种依赖注入方式你建议使用，构造器注入，还是 Setter 方法注入？](http://39sd.cn/201201042)

你两种依赖方式都可以使用， 构造器注入和 Setter 方法注入。最好的解决方案是用构造器参数实现强制依赖，setter 方法实现可选依赖。

### [16.解释对象/关系映射集成模块](http://39sd.cn/201201042)

Spring 通过提供 ORM 模块， 支持我们在直接 JDBC 之上使用一个对象/关系映射映射(ORM)工具，Spring  支持集成主流的 ORM 框架，如 Hiberate,JDO 和  iBATIS SQL Maps。Spring 的事务管理同样支持以上所有 ORM 框架及 JDBC。

### [17.解释 WEB  模块](http://39sd.cn/201201042)

Spring 的 WEB 模块是构建在 application context 模块基础之上， 提供一个适合web 应用的上下文。这个模块也包括支持多种面向 web 的任务，如透明地处理多个文件上传请求和程序级请求参数的绑定到你的业务对象。它也有对 Jakarta Struts 的支持。

#### [18.Spring 配置文件作用](http://39sd.cn/201201042)

Spring 配置文件是个 XML 文件， 这个文件包含了类信息， 描述了如何配置它们， 以及如何相互调用。

### [19.ApplicationContext 通常的实现是什么？](http://39sd.cn/201201042)

FileSystemXmlApplicationContext ： 此容器从一个 XML 文件中加载 beans 的定义， XML Bean 配置文件的全路径名必须提供给它的构造函数。

ClassPathXmlApplicationContext：此容器也从一个 XML 文件中加载 beans 的定义， 这里， 你需要正确设置 classpath 因为这个容器将在 classpath 里找bean 配置。

WebXmlApplicationContext：此容器加载一个 XML 文件，此文件定义了一个WEB 应用的所有 bean。

### [20.Bean 工厂和Application contexts有什么区别？](http://39sd.cn/201201042)

Application contexts 提供一种方法处理文本消息，一个通常的做法是加载文件资源（比如镜像），它们可以向注册为监听器的 bean 发布事件。另外，在容器或容器内的对象上执行的那些不得不由 bean 工厂以程序化方式处理的操作， 可以在Application contexts 中 以 声 明 的 方 式 处 理 。 

Application contexts 实 现 了MessageSource 接口，该接口的实现以可插拔的方式提供获取本地化消息的方法。

### [21.一个 Spring 的应用看起来象什么？](http://39sd.cn/201201042)

一个定义了一些功能的接口

这实现包括属性，它的 Setter ， getter 方法和函数等

Spring AOP

Spring 的 XML 配置文件

使用以上功能的客户端程序

### [22.什么是 Spring beans？](http://39sd.cn/201201042)

### [23.一个 Spring Bean 定义 包含什么？](http://39sd.cn/201201042)

### [24. spring 中的 bean 是线程安全的吗？](http://39sd.cn/201201042)

### [25. spring 支持几种 bean 的作用域？](http://39sd.cn/201201042)

### [26. spring 自动装配 bean 有哪些方式？](http://39sd.cn/201201042)

### [27. spring 事务实现方式有哪些？](http://39sd.cn/201201042)

### [28. 说一下 spring 的事务隔离？](http://39sd.cn/201201042)

### [29.什么是基于 Java 的 Spring 注解配置? 给一些注解的例子](http://39sd.cn/201201042)

### [30.什么是基于注解的容器配置？](http://39sd.cn/201201042)

### [31.怎样开启注解装配？](http://39sd.cn/201201042)

### [32.@Required 注解](http://39sd.cn/201201042)

### [33.@Autowired 注 解](http://39sd.cn/201201042)

### [34.@Qualifier 注 解](http://39sd.cn/201201042)

### [35.在 Spring 框架中如何更有效地使用 JDBC？](http://39sd.cn/201201042)

### [36.JdbcTemplate](http://39sd.cn/201201042)

### [37.Spring 对 DAO 的支持](http://39sd.cn/201201042)

### [38.使用 Spring 通过什么方式访问 Hibernate？](http://39sd.cn/201201042)

### [39.Spring 支持的 ORM](http://39sd.cn/201201042)

### [40.如何通过 HibernateDaoSupport 将 Spring 和 Hibernate 结合起来？](http://39sd.cn/201201042)

### [41.Spring 支持的事务管理类型](http://39sd.cn/201201042)

### [42.Spring 框架的事务管理有哪些优点？](http://39sd.cn/201201042)

### [43.你更倾向用那种事务管理类型？](http://39sd.cn/201201042)

### [44.什么是 Spring 的 MVC 框架？](http://39sd.cn/201201042)

### [45. 说一下 spring mvc 运行流程？](http://39sd.cn/201201042)

### [46. spring mvc 有哪些组件？](http://39sd.cn/201201042)

### [47. @RequestMapping 的作用是什么？](http://39sd.cn/201201042)

### [48. @Autowired 的作用是什么？](http://39sd.cn/201201042)

### [49.DispatcherServlet](http://39sd.cn/201201042)

### [50.WebApplicationContext](http://39sd.cn/201201042)

### [51.什么是 Spring MVC 框架的控制器？](http://39sd.cn/201201042)

### [52.@Controller 注 解](http://39sd.cn/201201042)

### [53.@RequestMapping 注 解](http://39sd.cn/201201042)

### [54.Aspect 切 面](http://39sd.cn/201201042)

### [55. 在 Spring AOP 中，关注点和横切关注的区别是什么？](http://39sd.cn/201201042)

### [56.连接点](http://39sd.cn/201201042)

### [57.通知](http://39sd.cn/201201042)

### [58.切点](http://39sd.cn/201201042)

### [59.什么是引入？](http://39sd.cn/201201042)

### [60.什么是目标对象？](http://39sd.cn/201201042)

### [61.什么是代理？](http://39sd.cn/201201042)

### [62.有几种不同类型的自动代理？](http://39sd.cn/201201042)

### [63.什么是织入。什么是织入应用的不同点？](http://39sd.cn/201201042)

### [64.解释基于 XML Schema 方式的切面实现](http://39sd.cn/201201042)

### [65.解释基于注解的切面实现](http://39sd.cn/201201042)

### **下载链接**：[**博主已将以上这些面试题整理成了一个面试手册，是PDF版的**](https://gitee.com/woniu201/interview-reference/blob/master/daan.md)